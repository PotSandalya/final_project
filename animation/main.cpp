#include <SFML/Graphics.hpp>
#include <iostream>
// ���������� ��� �������� ���
#include <chrono>
#include <thread>
#include <vector>
#include "Circle.hpp"

using namespace std::chrono_literals;

int main()
{
    srand(time(0)); // ��������� ��������� �����

    // �������� ������� � ���������� �������� �����
    std::vector<A::Circle*> shapse;
    for (int i = -200; i <= 300; i += 60)
    {
        shapse.push_back(new A::Circle(900, i, 2, rand() % 13 + 5));  // �������� ���������� �� ��������� 
    }


    //�������� � �������� �������� �� ����
    sf::Texture cosmos;
    sf::Texture Nlo;
    if ((!cosmos.loadFromFile("img\\1sky.jpg")) || (!Nlo.loadFromFile("img\\nloo.png")))
    {
        std::cout << "ERROR: File .jpg not found" << std::endl;
        return -1;
    }

    sf::Sprite background(cosmos);   // Sprite ��� ��������� ������ ���������� ������������ � ��������� (�� ����� �������� 1Sky � �������� �� ������ ����� �������)
    sf::Sprite nlo(Nlo);

    // �������� ���� ������ ��������
    sf::RenderWindow window(sf::VideoMode(845, 525), L"������");

    // ���������� ���
    float x_Nlo = 860;

    while (window.isOpen())
    {
        // �������� ������, ������� ��������� �������� ���� ��������� ��� ������� �� �������� ����
        sf::Event event;
        while (window.pollEvent(event))
        {
            if (event.type == sf::Event::Closed)
                window.close();
        }

        // ��������� ���������� ���
        x_Nlo = x_Nlo - 6;
        nlo.setPosition(x_Nlo, -10);

        // ��������� ��������� ��� �������� �����
        for (const auto& circle : shapse)
        {
            if ((circle->Getx() < -1000 || (circle->Gety() > 340)))
            {
                circle->Spawn(rand() % 300 - 200);
            }
            circle->Move();
        }



        window.clear();

        // ��� (��� ������ ����������� ������)
        window.draw(background);


        // ��������� �������� ������
        for (const auto& circle : shapse)
            window.draw(*circle->Get());

        // ��������� ���
        window.draw(nlo);


        window.display();

        std::this_thread::sleep_for(40ms);

        // ���� ��� ����� �� ����� ������, �� ��������� ���������
        if (x_Nlo < -450)
        {
            for (const auto& circle : shapse)
                delete circle;
            shapse.clear();

            std::cout << "end" << std::endl;
            return 1;
        }
    }

    return 0;
}