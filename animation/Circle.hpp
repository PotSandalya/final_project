#pragma once

#include <SFML/Graphics.hpp>

namespace A
{
    class Circle
    {
    public:
        Circle(int x, int y, float r, float speed);

        ~Circle();


        sf::CircleShape* Get();

        void Move();

        void Spawn(int y);


        int Getx();
        int Gety();

    private:
        int m_x;
        int m_y;
        float m_r;
        float m_speed;
        sf::CircleShape* m_shape;
    };
}


