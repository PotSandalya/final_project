#include "Circle.hpp"

namespace A
{

    Circle::Circle(int x, int y, float r, float speed)
    {
        m_x = x;
        m_y = y;
        m_r = r;
        m_speed = speed;
        m_shape = new sf::CircleShape(m_r);
        m_shape->setFillColor(sf::Color::White);
        m_shape->setPosition(m_x, m_y);


    }
    Circle::~Circle()
    {
        delete m_shape;
    }

    sf::CircleShape* Circle::Get() { return m_shape; }

    void Circle::Move()
    {
        m_x = m_x - m_speed * 2;
        m_y = m_y + m_speed / 2;
        m_shape->setPosition(m_x, m_y);
    }
    void Circle::Spawn(int y)
    {
        m_x = 1200;   // �� ������
        m_y = y;
        m_shape->setPosition(m_x, m_y);
    }

    int Circle::Getx() { return m_x; }; //���������� ���������� �
    int Circle::Gety() { return m_y; }; //���������� ���������� y

}